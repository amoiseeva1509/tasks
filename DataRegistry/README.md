# DataRegistry

The program accepts 3 input files:metadata.txt, data.txt, queries.txt. 
The file metadata.txt contains description of data properties.
Each metadata is on new line and has format: <br/> "name of metadata: option1, option2, ..., optionN". 
<br/>The file data.txt contains data records described based on metadata.
Each data is on new line and has format: <br/> ""name of data", trait1:option of trait1, trait2:option of trait2, ..., traitN:option of traitN". 
<br/> The file queries.txt contains queries. 
Each query on new line and has format: <br/> "trait1:option of trait1, trait2:option of trait2, ..., traitN:option of traitN". 
<br/> The program reads data.txt and finds all strings according to queries in queries.txt. 
## How to build

```
mvn clean install
```

## How to run locally

```
 java -jar ./target/DataRegistry-0.0.1.jar --metadata=/foo/bar/metadata.txt --data=/foo/bar/data1.txt --queries=/foo/bar/queries.txt
```