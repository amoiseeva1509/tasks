package io.neolab.model;

import java.util.Objects;

public class Result {

    private String query;
    private Long count;

    public Result(String query, Long count) {
        this.query = query;
        this.count = count;
    }

    public String getQuery() {
        return query;
    }

    public Long getCount() {
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return Objects.equals(query, result.query) &&
                Objects.equals(count, result.count);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, count);
    }

    @Override
    public String toString() {
        return "Result{" +
                "query='" + query + '\'' +
                ", count=" + count +
                '}';
    }
}
