package io.neolab.model;

import java.util.Set;

public class Data {

    private String name;
    private Set<Metadata> metadata;

    public Data(String name, Set<Metadata> metadata) {
        this.name = name;
        this.metadata = metadata;
    }

    public String getName() {
        return name;
    }

    public Set<Metadata> getMetadata() {
        return metadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Data data = (Data) o;

        if (name != null ? !name.equals(data.name) : data.name != null) return false;
        return metadata != null ? metadata.equals(data.metadata) : data.metadata == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
        return result;
    }
}
