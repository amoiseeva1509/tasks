package io.neolab;

import io.neolab.database.Database;
import io.neolab.database.InMemoryDatabase;
import io.neolab.model.Metadata;
import io.neolab.model.Result;
import io.neolab.parser.Parser;
import io.neolab.service.DataLoader;
import io.neolab.service.FileBasedDataLoader;
import io.neolab.service.FileBasedQueryBuilder;
import io.neolab.service.FileBasedMetadataLoader;
import io.neolab.service.MetadataLoader;
import io.neolab.service.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws IOException {

        MetadataLoader metadataLoader = new FileBasedMetadataLoader(Parser.getMetadataPath(Arrays.asList(args)));
        DataLoader dataLoader = new FileBasedDataLoader(Parser.getDataPath(Arrays.asList(args)));
        QueryBuilder queryBuilder = new FileBasedQueryBuilder(Parser.getQueriesPath(Arrays.asList(args)));

        Set<Metadata> metadataSet = metadataLoader.load();

        Database db = new InMemoryDatabase();
        db.insertData(dataLoader.load(metadataSet));
        List<Result> results = db.query(queryBuilder.build(metadataSet));

        results.forEach(result -> LOGGER.info(result.toString()));
    }


}
