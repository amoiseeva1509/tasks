package io.neolab.service;

import io.neolab.model.Data;
import io.neolab.model.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileBasedDataLoader implements DataLoader {

    private static final String SEPARATOR = ",";
    private static final String SEPARATOR_BETWEEN_NAME_AND_VALUE = ":";
    private static final int NAME_AND_TRAITS_MIN_COUNT = 2;
    private static final int NUMBER_OF_NAME = 0;
    private static final int NUMBER_OF_VALUES = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileBasedDataLoader.class);

    private String fileName;

    public FileBasedDataLoader(String fileName) {
        this.fileName = fileName;
    }

    public List<Data> load(Set<Metadata> metadata) throws IOException {
        LOGGER.info("Start load {}.", fileName);
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(line -> parseLine(line))
                    .filter(array -> isValidArray(array))
                    .map(array -> new Data(array[NUMBER_OF_NAME], buildMetadata(metadata, array)))
                    .collect(Collectors.toList());
        }
    }

    private Set<Metadata> buildMetadata(Set<Metadata> metadata, String[] data) {
        return Stream.of(data)
                .skip(1)
                .map(value -> value.split(SEPARATOR_BETWEEN_NAME_AND_VALUE))
                .filter(array -> isValidMetadata(metadata, array))
                .map(array -> new Metadata(array[NUMBER_OF_NAME].trim(), array[NUMBER_OF_VALUES].trim()))
                .collect(Collectors.toSet());
    }

    private String[] parseLine(String line) {
        String[] array = line.split(SEPARATOR);
        if (!isValidArray(array)) {
            LOGGER.warn("String \"{}\" is invalid. It will be skipped.", line);
        }

        return array;
    }

    private boolean isValidMetadata(Set<Metadata> metadataSet, String[] metadata) {
        return metadataSet.contains(new Metadata(metadata[NUMBER_OF_NAME].trim(), metadata[NUMBER_OF_VALUES].trim()));
    }

    private boolean isValidArray(String[] array) {
        return array.length >= NAME_AND_TRAITS_MIN_COUNT && !array[NUMBER_OF_NAME].trim().equals("");
    }


}
