package io.neolab.service;

import io.neolab.model.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileBasedQueryBuilder implements QueryBuilder {

    private static final String SEPARATOR_BETWEEN_QUERIES = ",";
    private static final String SEPARATOR_BETWEEN_NAME_AND_VALUE = ":";
    private static final int MIN_COUNT_OF_TRAITS = 1;
    private static final int NUMBER_OF_NAME = 0;
    private static final int NUMBER_OF_VALUE = 1;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileBasedQueryBuilder.class);

    private String fileName;

    public FileBasedQueryBuilder(String fileName) {
        this.fileName = fileName;
    }

    public List<Set<Metadata>> build(Set<Metadata> metadata) throws IOException {
        LOGGER.info("Start load {}.", fileName);
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(line -> line.split(SEPARATOR_BETWEEN_QUERIES))
                    .map(array -> buildMetadata(metadata, array))
                    .filter(set -> !set.isEmpty())
                    .collect(Collectors.toList());
        }
    }

    private Set<Metadata> buildMetadata(Set<Metadata> metadata, String[] query) {
        return Stream.of(query)
                .map(value -> parseLine(value))
                .filter(array -> isValidArray(array) && isValidMetadata(metadata, array))
                .map(array -> new Metadata(array[NUMBER_OF_NAME].trim(), array[NUMBER_OF_VALUE].trim()))
                .collect(Collectors.toSet());
    }

    private boolean isValidMetadata(Set<Metadata> metadataSet, String[] metadata) {
        return metadataSet.contains(new Metadata(metadata[NUMBER_OF_NAME].trim(), metadata[NUMBER_OF_VALUE].trim()));
    }

    private boolean isValidArray(String[] array) {
        return array.length > MIN_COUNT_OF_TRAITS;
    }

    private String[] parseLine(String line) {
        String[] array = line.split(SEPARATOR_BETWEEN_NAME_AND_VALUE);
        if (!isValidArray(array)) {
            LOGGER.warn("String \"{}\" is invalid. It will be skipped.", line);
        }

        return array;
    }
}
