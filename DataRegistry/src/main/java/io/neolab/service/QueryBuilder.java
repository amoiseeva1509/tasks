package io.neolab.service;

import io.neolab.model.Metadata;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface QueryBuilder {

    List<Set<Metadata>> build(Set<Metadata> metadata) throws IOException;
}
