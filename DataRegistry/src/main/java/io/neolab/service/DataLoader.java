package io.neolab.service;

import io.neolab.model.Data;
import io.neolab.model.Metadata;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface DataLoader {

    List<Data> load(Set<Metadata> metadata) throws IOException;
}
