package io.neolab.service;

import io.neolab.model.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileBasedMetadataLoader implements MetadataLoader {

    private static final String SEPARATOR_BETWEEN_NAME_AND_VALUES = ":";
    private static final String SEPARATOR_BETWEEN_VALUES = ",";
    private static final int NUMBER_OF_KEY = 0;
    private static final int NUMBER_OF_VALUES = 1;
    private static final int KEY_AND_VALUES_COUNT = 2;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileBasedMetadataLoader.class);

    private String fileName;

    public FileBasedMetadataLoader(String fileName) {
        this.fileName = fileName;
    }

    public Set<Metadata> load() throws IOException {
        LOGGER.info("Start load {}.", fileName);
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(line -> parseLine(line))
                    .filter(array -> isValidArray(array))
                    .flatMap(array -> buildMetadata(array))
                    .collect(Collectors.toSet());
        }
    }

    private Stream<Metadata> buildMetadata(String[] metadata) {
        return Stream.of(metadata[NUMBER_OF_VALUES].split(SEPARATOR_BETWEEN_VALUES))
                .filter(el -> !el.trim().equals(""))
                .map(el -> new Metadata(metadata[NUMBER_OF_KEY].trim(), el.trim()));
    }

    private boolean isValidArray(String[] array) {
        return array.length == KEY_AND_VALUES_COUNT && !array[NUMBER_OF_KEY].trim().equals("");
    }

    private String[] parseLine(String line) {
        String[] array = line.split(SEPARATOR_BETWEEN_NAME_AND_VALUES);
        if (!isValidArray(array)) {
            LOGGER.warn("String \"{}\" is invalid. It will be skipped.", line);
        }

        return array;
    }
}
