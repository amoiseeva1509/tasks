package io.neolab.service;

import io.neolab.model.Metadata;

import java.io.IOException;
import java.util.Set;

public interface MetadataLoader {

    Set<Metadata> load() throws IOException;
}
