package io.neolab.parser;

import java.util.List;
import java.util.function.Predicate;

public class Parser {

    private static final String METADATA = "metadata";
    private static final String DATA = "data";
    private static final String QUERIES = "queries";

    private static final String SEPARATOR = "=";
    private static final int PATH_NUMBER = 1;
    private static final int NAME_AND_PATH = 2;


    public static String getMetadataPath(List<String> paths) {
        return getElementPath(paths, arg -> arg.contains(METADATA));
    }

    public static String getDataPath(List<String> paths) {
        return getElementPath(paths, arg -> arg.contains(DATA) && !arg.contains(METADATA));
    }

    public static String getQueriesPath(List<String> paths) {
        return getElementPath(paths, arg -> arg.contains(QUERIES));
    }

    private static String getElementPath(List<String> paths, Predicate<String> predicate) {
        return paths.stream()
                .filter(predicate)
                .map(string -> string.split(SEPARATOR))
                .filter(array -> array.length == NAME_AND_PATH)
                .map(array -> array[PATH_NUMBER])
                .findFirst()
                .orElse("");
    }
}
