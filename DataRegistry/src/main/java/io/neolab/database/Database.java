package io.neolab.database;

import io.neolab.model.Data;
import io.neolab.model.Metadata;
import io.neolab.model.Result;

import java.util.List;
import java.util.Set;

public interface Database {

    void insertData(List<Data> dataList);

    List<Result> query(List<Set<Metadata>> queries);
}
