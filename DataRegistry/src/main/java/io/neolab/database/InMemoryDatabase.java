package io.neolab.database;

import io.neolab.model.Metadata;
import io.neolab.model.Data;
import io.neolab.model.Result;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class InMemoryDatabase implements Database {

    private List<Data> data;

    public InMemoryDatabase() {

    }

    @Override
    public void insertData(List<Data> data) {
        this.data = data;
    }

    @Override
    public List<Result> query(List<Set<Metadata>> queries) {
        return queries.stream()
                .map(query -> new Result(query.toString(), count(data, query)))
                .collect(Collectors.toList());
    }

    private Long count(List<Data> dataList, Set<Metadata> metadataSet) {
        return dataList.stream().filter(data -> data.getMetadata().containsAll(metadataSet)).count();
    }
}
