package io.neolab;

import io.neolab.model.Metadata;
import io.neolab.model.Data;
import io.neolab.model.Result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BaseServiceTest {

    public Set<Metadata> buildMetadata() {
        Set<Metadata> metadata = new HashSet<>();

        metadata.add(new Metadata("WEIGHT", "LOW"));
        metadata.add(new Metadata("WEIGHT", "AVERAGE"));
        metadata.add(new Metadata("WEIGHT", "HIGH"));
        metadata.add(new Metadata("HEIGHT", "LOW"));
        metadata.add(new Metadata("HEIGHT", "MIDDLE"));
        metadata.add(new Metadata("HEIGHT", "HIGH"));
        metadata.add(new Metadata("FOOD", "CARNIVORE"));
        metadata.add(new Metadata("FOOD", "OMNIVORE"));
        metadata.add(new Metadata("FOOD", "HERBIVORE"));

        return metadata;
    }

    public List<Set<Metadata>> buildQueryMetadata() {

        Set<Metadata> metadata1 = new HashSet<>();

        metadata1.add(new Metadata("WEIGHT", "HIGH"));
        metadata1.add(new Metadata("FOOD", "CARNIVORE"));

        Set<Metadata> metadata2 = new HashSet<>();

        metadata2.add(new Metadata("WEIGHT", "HIGH"));

        return Arrays.asList(metadata1, metadata2);
    }

    public List<Result> buildResults() {
        List<Result> results = new ArrayList<>();

        results.add(new Result("[FOOD:CARNIVORE, WEIGHT:HIGH]", 3l));
        results.add(new Result("[WEIGHT:HIGH]", 4l));

        return results;
    }

    public List<Data> buildData() {
        List<Data> data = new ArrayList<>();

        Set<Metadata> metadata1 = new HashSet<>();
        metadata1.add(new Metadata("WEIGHT", "HIGH"));
        metadata1.add(new Metadata("HEIGHT", "HIGH"));
        metadata1.add(new Metadata("FOOD", "OMNIVORE"));

        Data data1 = new Data("\"bear\"", metadata1);

        Set<Metadata> metadata2 = new HashSet<>();
        metadata2.add(new Metadata("WEIGHT", "LOW"));
        metadata2.add(new Metadata("HEIGHT", "LOW"));
        metadata2.add(new Metadata("FOOD", "HERBIVORE"));

        Data data2 = new Data("\"rabbit\"", metadata2);

        Set<Metadata> metadata3 = new HashSet<>();
        metadata3.add(new Metadata("WEIGHT", "HIGH"));
        metadata3.add(new Metadata("HEIGHT", "LOW"));
        metadata3.add(new Metadata("FOOD", "CARNIVORE"));

        Data data3 = new Data("\"crocodile\"", metadata3);

        Set<Metadata> metadata4 = new HashSet<>();
        metadata4.add(new Metadata("WEIGHT", "HIGH"));
        metadata4.add(new Metadata("HEIGHT", "MIDDLE"));
        metadata4.add(new Metadata("FOOD", "CARNIVORE"));

        Data data4 = new Data("\"lion\"", metadata4);

        Set<Metadata> metadata5 = new HashSet<>();
        metadata5.add(new Metadata("WEIGHT", "HIGH"));
        metadata5.add(new Metadata("HEIGHT", "MIDDLE"));
        metadata5.add(new Metadata("FOOD", "CARNIVORE"));

        Data data5 = new Data("\"tiger\"", metadata5);

        data.add(data1);
        data.add(data2);
        data.add(data3);
        data.add(data4);
        data.add(data5);

        return data;
    }
}
