package io.neolab.service;

import io.neolab.BaseServiceTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertTrue;

public class FileBasedQueryBuilderTest extends BaseServiceTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    private File tempFile;

    private FileBasedQueryBuilder fileBasedQueryBuilder;

    @Before
    public void setUp() throws IOException {
        tempFile = testFolder.newFile("file.txt");
        try (FileWriter writer = new FileWriter(tempFile)) {
            writer.write("WEIGHT:HIGH\n");
            writer.write("FOOD:CARNIVORE, WEIGHT:HIGH\n");
            writer.write("invalid string\n");
        }
        fileBasedQueryBuilder = new FileBasedQueryBuilder(tempFile.getAbsolutePath());
    }

    @Test
    public void testSuccessIfFileDataIsCorrect() throws IOException {
        assertTrue(buildQueryMetadata().containsAll(fileBasedQueryBuilder.build(buildMetadata())));
    }

    @Test(expected = IOException.class)
    public void testFailedIfFileIsNotFind() throws IOException {
        FileBasedQueryBuilder loader = new FileBasedQueryBuilder("invalidFile.txt");
        loader.build(Collections.emptySet());
    }
}
