package io.neolab.service;

import io.neolab.BaseServiceTest;
import io.neolab.database.Database;
import io.neolab.database.InMemoryDatabase;
import io.neolab.model.Result;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class InMemoryDatabaseTest extends BaseServiceTest {

    @Test
    public void successfulCalculateAmountOfAnimalsByQueries() {

        Database service = new InMemoryDatabase();
        service.insertData(buildData());
        List<Result> results = service.query(buildQueryMetadata());

        assertTrue(buildResults().equals(results));
    }


}
