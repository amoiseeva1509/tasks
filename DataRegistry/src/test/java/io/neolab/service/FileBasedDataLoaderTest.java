package io.neolab.service;

import io.neolab.BaseServiceTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class FileBasedDataLoaderTest extends BaseServiceTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    private File tempFile;

    private FileBasedDataLoader fileBasedDataLoader;

    @Before
    public void setUp() throws IOException {
        tempFile = testFolder.newFile("file.txt");
        try (FileWriter writer = new FileWriter(tempFile)) {
            writer.write("\"bear\", WEIGHT:HIGH, HEIGHT:HIGH, FOOD:OMNIVORE\n");
            writer.write("\"rabbit\", WEIGHT:LOW, HEIGHT:LOW, FOOD:HERBIVORE\n");
            writer.write("\"crocodile\", WEIGHT:HIGH, HEIGHT:LOW, FOOD:CARNIVORE\n");
            writer.write("\"lion\", WEIGHT:HIGH, HEIGHT:MIDDLE, FOOD:CARNIVORE\n");
            writer.write("\"tiger\", WEIGHT:HIGH, HEIGHT:MIDDLE, FOOD:CARNIVORE\n");
            writer.write("invalid string\n");
        }
        fileBasedDataLoader = new FileBasedDataLoader(tempFile.getAbsolutePath());
    }

    @Test
    public void testSuccessIfFileDataIsCorrect() throws IOException {
        assertTrue(buildData().equals(fileBasedDataLoader.load(buildMetadata())));
    }

    @Test(expected = IOException.class)
    public void testFailedIfFileIsNotFind() throws IOException {
        FileBasedDataLoader loader = new FileBasedDataLoader("invalidFile.txt");
        loader.load(buildMetadata());
    }
}
