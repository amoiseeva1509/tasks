package io.neolab.service;

import io.neolab.BaseServiceTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class FileBasedMetadataLoaderTest extends BaseServiceTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    private File tempFile;

    private FileBasedMetadataLoader fileBasedMetadataLoader;

    @Before
    public void setUp() throws IOException {
        tempFile = testFolder.newFile("file.txt");
        try (FileWriter writer = new FileWriter(tempFile)) {
            writer.write("WEIGHT: LOW, AVERAGE, HIGH\n");
            writer.write("HEIGHT: LOW, MIDDLE, HIGH\n");
            writer.write("FOOD: CARNIVORE, OMNIVORE, HERBIVORE\n");
            writer.write(": RUSSIA\n");
            writer.write("invalid string\n");
        }
        fileBasedMetadataLoader = new FileBasedMetadataLoader(tempFile.getAbsolutePath());
    }

    @Test
    public void testSuccessIfFileDataIsCorrect() throws IOException {
        assertTrue(buildMetadata().equals(fileBasedMetadataLoader.load()));
    }

    @Test(expected = IOException.class)
    public void testFailedIfFileIsNotFind() throws IOException {
        FileBasedMetadataLoader loader = new FileBasedMetadataLoader("invalidFile.txt");
        loader.load();
    }
}
