# Numerics

Service to perform functions for numerics, for example, to get 3 max values in array or to find sum of numerics for numeric.

## How to build

```
mvn clean install
```

## How to run locally

```
java -jar ./target/Numerics-0.0.1.jar
```