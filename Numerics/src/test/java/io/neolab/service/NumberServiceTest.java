package io.neolab.service;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class NumberServiceTest {

    private NumericService service = new NumericService();//!!!!

    @Test
    public void testGetMaxElements() {
        assertEquals(Arrays.asList(98, 12, 5), service.getMaxElements(Arrays.asList(1, 2, 5, 12, 98, 0, 3, 12)));
    }

    @Test
    public void testGetMaxElementsIfArrayIsTooSmall() {
        assertEquals(Arrays.asList(1, -2), service.getMaxElements(Arrays.asList(1, -2)));
    }

    @Test
    public void testGetMaxElementsIfArrayIsEmpty() {
        assertEquals(Arrays.asList(), service.getMaxElements(Arrays.asList()));
    }

    @Test
    public void testSumOfNumericOfInteger() {
        assertEquals(6, service.sumOfNumericOfInteger(123));
    }
}
