package io.neolab;

import io.neolab.service.NumericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class Application {

    static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 5, 12, 98, 0, 3, 12);
        NumericService service = new NumericService();
        LOGGER.info("Max three elements from {} is {}.", list, service.getMaxElements(list));
        LOGGER.info("sum of numeric of {} is {}.", 56, service.sumOfNumericOfInteger(56));//gufoyseoriew

    }
}
