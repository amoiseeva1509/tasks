package io.neolab.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class NumericService {

    public List<Integer> getMaxElements(List<Integer> list) {
        return list.stream()
                .distinct()
                .sorted(Comparator.reverseOrder())
                .limit(3)
                .collect(Collectors.toList());
    }

    public int sumOfNumericOfInteger(int numeric) {
        int sum = 0;
        while (numeric > 0){
            sum = sum + (numeric % 10);
            numeric /= 10;
        }
        return sum;
    }
}
